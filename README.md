# pesol-vscode-odoo-snippets

Snippets de Odoo para VSCode, migrados desde Atom.

Para utilizar empezar a escribir 'o' en ficheros python o xml.

## Instalación

Descargar git@gitlab.com:pesol/pesol-vscode-odoo-snippets.git

Copiar el directorio pesol-vscode-odoo-snippets en  `~/.vscode/extensions`

Al reiniciar se recargará e instalara la extensión

## TODO

- Añadir RST como dependencia para usar sus snippets